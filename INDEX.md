# Package Tools

# PkgTools

Some general package information and management utilities for FreeDOS

# Contributing

**Would you like to contribute to FreeDOS?** The programs listed here are a great place to start. Most of these do not have a maintainer anymore and need your help to make them better. Here's how to get started:

* __Maintainers__: Let us know if you'd like to take on one of these programs, and we can provide access to the source code repository here. Please use the FreeDOS package structure when you make new releases, including all executables, source code, and metadata.

* __New developers__: We can extend access for you to track issues here.

* __Translators__: Please submit to the [FD-NLS Project](https://github.com/shidel/fd-nls).

_*Make sure to check all source code licenses, especially for any code you might reuse from other projects to improve these programs. Note that not all open source licenses are the same or compatible with one another. (For example, you cannot reuse code covered under the GNU GPL in a program that uses the BSD license.)_

## PKGTOOLS.LSM

<table>
<tr><td>title</td><td>Package Tools</td></tr>
<tr><td>version</td><td>2021-12-31</td></tr>
<tr><td>entered&nbsp;date</td><td>2021-12-31</td></tr>
<tr><td>description</td><td>Display information on installed packages</td></tr>
<tr><td>summary</td><td>Utilities to create and display information about installed packages for FreeDOS. Eventually, utilities to relocate, delete and perform other general package maintenance on packages may be added.</td></tr>
<tr><td>keywords</td><td>dos 16 bit, asm, pascal</td></tr>
<tr><td>author</td><td>Jerome Shidel &lt;jerome _AT_ shidel.net&gt;</td></tr>
<tr><td>maintained&nbsp;by</td><td>Jerome Shidel &lt;jerome _AT_ shidel.net&gt;</td></tr>
<tr><td>primary&nbsp;site</td><td>https://fd.lod.bz/repos/current/pkg-html/pkgtools.html</td></tr>
<tr><td>alternate&nbsp;site</td><td>https://gitlab.com/FDOS/util/pkgtools</td></tr>
<tr><td>original&nbsp;site</td><td>https://github.com/shidel/PkgTools</td></tr>
<tr><td>platforms</td><td>DOS</td></tr>
<tr><td>copying&nbsp;policy</td><td>[Mozilla Public License Version 2.0](LICENSE)</td></tr>
</table>
